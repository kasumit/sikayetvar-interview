import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import utils.StringUtils;

public class StringUtilsTest {

    @Test
    @DisplayName("when given string parameter is empty string(with 0 length but not null) then function returns true")
    public void given_empty_string_when_isNullOrEmpty_then_true() {
        String objectUnderTest = "";
        Assertions.assertTrue(StringUtils.isNullOrEmpty(objectUnderTest));
    }

    @Test
    @DisplayName("when given string parameter is null then function returns true")
    public void given_null_string_when_isNullOrEmpty_then_true() {
        String objectUnderTest = null;
        Assertions.assertTrue(StringUtils.isNullOrEmpty(objectUnderTest));
    }
}
