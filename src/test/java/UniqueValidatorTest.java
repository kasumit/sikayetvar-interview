import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import uniqueValidator.IUniqueValidator;
import uniqueValidator.UniqueValidatorWithDataStructureStrategy;
import uniqueValidator.UniqueValidatorWithNoDataStructureStrategy;

public class UniqueValidatorTest {

    private IUniqueValidator uniqueValidatorWithDataStructure;
    private IUniqueValidator uniqueValidatorWithNoDataStructure;


    @BeforeEach
    public void setUp() {
        this.uniqueValidatorWithDataStructure = new UniqueValidatorWithDataStructureStrategy();
        this.uniqueValidatorWithNoDataStructure = new UniqueValidatorWithNoDataStructureStrategy();
    }

    @Test
    @DisplayName("when given empty string(with 0 length but not null) value to parameter of uniqueValidator.IUniqueValidator::isUnique function then returns false")
    public void given_empty_string_when_isUnique_then_true() {
        String objectUnderTest = "";
        Assertions.assertFalse(uniqueValidatorWithDataStructure.isUnique(objectUnderTest));
        Assertions.assertFalse(uniqueValidatorWithNoDataStructure.isUnique(objectUnderTest));
    }

    @Test
    @DisplayName("when given null value to parameter of uniqueValidator.IUniqueValidator::isUnique function then returns false")
    public void given_null_string_when_isUnique_then_true() {
        String objectUnderTest = null;
        Assertions.assertFalse(uniqueValidatorWithDataStructure.isUnique(objectUnderTest));
        Assertions.assertFalse(uniqueValidatorWithNoDataStructure.isUnique(objectUnderTest));
    }


    @Test
    @DisplayName("when given a string that not contains unique character to parameter of uniqueValidator.IUniqueValidator::isUnique function then returns false")
    public void given_not_unique_character_string_when_isUnique_then_true() {
        String objectUnderTest = "TREE";
        Assertions.assertFalse(uniqueValidatorWithDataStructure.isUnique(objectUnderTest));
        Assertions.assertFalse(uniqueValidatorWithNoDataStructure.isUnique(objectUnderTest));
    }

    @Test
    @DisplayName("when given a string that contains unique character to parameter of uniqueValidator.IUniqueValidator::isUnique function then returns true")
    public void given_unique_character_string_when_isUnique_then_true() {
        String objectUnderTest = "HOME";
        Assertions.assertTrue(uniqueValidatorWithDataStructure.isUnique(objectUnderTest));
        Assertions.assertTrue(uniqueValidatorWithNoDataStructure.isUnique(objectUnderTest));
    }

    @Test
    @DisplayName("when given a single character string that contains unique character to parameter of uniqueValidator.IUniqueValidator::isUnique function then returns true")
    public void given_single_character_string_when_isUnique_then_true() {
        String objectUnderTest = "H";
        Assertions.assertTrue(uniqueValidatorWithDataStructure.isUnique(objectUnderTest));
        Assertions.assertTrue(uniqueValidatorWithNoDataStructure.isUnique(objectUnderTest));
    }
}
