import exception.NegativeNumberException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import utils.Math;
import utils.StringUtils;

public class MathTest {


    @Test
    @DisplayName("when given positive 2 integer parameters Math::sum function then returns sum of parameters")
    public void given_two_positive_integer_when_sum_then_output_sum_of_integers() {
        Assertions.assertEquals(8, Math.sum(3, 5));
    }

    @Test
    @DisplayName("when given one positive and 1 negative integer parameter Math::sum function then returns sum of parameters")
    public void given_one_positive_one_negative_integer_when_sum_then_output_sum_of_integers() {
        Assertions.assertEquals(2, Math.sum(-3, 5));
    }


    @Test
    @DisplayName("when given 2 negative integer parameters Math::sum function then returns sum of parameters")
    public void given_two_negative_integer_when_sum_then_output_sum_of_integers() {
        Assertions.assertEquals(-8, Math.sum(-3, -5));
    }



    @Test
    @DisplayName("when given positive 2 integer parameters Math::sumPositives function then returns sum of parameters")
    public void given_two_positive_integer_when_sumPositives_then_output_sum_of_integers() {
        Assertions.assertEquals(8, Math.sum(3, 5));
    }

    @Test
    @DisplayName("when given one positive and 1 negative integer parameter Math::sumPositives function then throws NegativeNumberException")
    public void given_one_positive_one_negative_integer_when_sumPositives_then_throws_NegativeNumberException() {
        Assertions.assertThrows(NegativeNumberException.class, () -> Math.sumPositives(-3, 5));
    }


    @Test
    @DisplayName("when given 2 negative integer parameters Math::sumPositives function then throws NegativeNumberException")
    public void given_two_negative_integer_when_sum_then_then_throws_NegativeNumberException() {
        Assertions.assertThrows(NegativeNumberException.class, () -> Math.sumPositives(-3, -5));    }

}
