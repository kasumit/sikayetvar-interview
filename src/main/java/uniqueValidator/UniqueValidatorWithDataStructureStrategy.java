package uniqueValidator;

import utils.StringUtils;

import java.util.HashSet;
import java.util.Set;

public class UniqueValidatorWithDataStructureStrategy implements IUniqueValidator {

    /**
     * Function returns true if given parameter is not null or empty nor contains same character only one times in full string.
     * Function returns false any other cases.
     *
     * the algorithms works as follows:
     *  - check given str is null or empty if it is return false
     *  - add each character of given string in a set, before adding check if the character exist in set
     *  - if character exist in set return false immediately
     *
     * @param str
     * @return
     */
    public boolean isUnique(String str) {
        if (StringUtils.isNullOrEmpty(str))
            return false;

        Set<Character> existChars = new HashSet<>();

        for (Character ch : str.toCharArray()) {
            if (existChars.contains(ch))
                return false;

            existChars.add(ch);
        }

        return true;
    }



}
