package uniqueValidator;

public interface IUniqueValidator {

    boolean isUnique(String str);
}
