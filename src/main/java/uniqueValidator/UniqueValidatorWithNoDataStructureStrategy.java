package uniqueValidator;

import utils.StringUtils;

import java.util.HashSet;
import java.util.Set;

public class UniqueValidatorWithNoDataStructureStrategy implements IUniqueValidator {

    /**
     * Function returns true if given parameter is not null or empty nor contains same character only one times in full string.
     * Function returns false any other cases.
     *
     * the algorithms works as follows:
     *  - check given str is null or empty if it is return false
     *  - iterate a loop for all indexes of given str
     *  - generate a sub string with not including the current index
     *  - check that current index character is contained in sub string
     *  - if it is contained return false immediately
     *  - end of the loop return true
     *
     * @param str
     * @return
     */
    public boolean isUnique(String str) {
        if (StringUtils.isNullOrEmpty(str))
            return false;

        for (int i = 0; i < str.length(); i++) {

            String subString = str.substring(i + 1);

            Character currentChar = str.charAt(i);
            if (subString.contains(String.valueOf(currentChar)))
                return false;
        }

        return true;

    }



}
