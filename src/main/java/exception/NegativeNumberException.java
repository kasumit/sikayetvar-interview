package exception;

/**
 * Thrown when an exceptional negative number exist.
 * For example, requested parameter is positive but given is negative
 * then throw this exception
 *
 */

public class NegativeNumberException extends RuntimeException {
    public NegativeNumberException(String message) {
        super(message);
    }
}
