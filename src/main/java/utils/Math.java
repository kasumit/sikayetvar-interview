package utils;

import exception.NegativeNumberException;

public class Math {

    /**
     * The function sums 2 integers with bitwise operations recursively.
     * XOR(^) operator gives the uncommon bits of both numbers
     * AND(&) operator gives the common bits of both numbers
     *
     * the algorithms works as follows:
     *  - get common and uncommon bits
     *  - if common bit is 0
     *  - then return the uncommon bits as result
     *  - else shift the common bits to reduce
     *  - call sum recursively until the there is no common bits exist
     *
     * @param num1
     * @param num2
     * @return
     *
     * ref: https://prismoskills.appspot.com/lessons/Bitwise_Operators/Sum_using_only_bitwise_ops.jsp
     */
    public static int sum(int num1, int num2) {

        int unCommonBits = num1 ^ num2;
        int commonBits = num1 & num2;

        if (commonBits == 0)
            return unCommonBits;

        int shiftedCommonBits = commonBits << 1;
        return sum(unCommonBits, shiftedCommonBits);
    }

    /**
     * The function sums 2 positive integers with bitwise operations.
     * If any parameter is negative throws NegativeNumberException exception

     * @param num1
     * @param num2
     * @return
     * @throws NegativeNumberException
     *
     * ref: https://prismoskills.appspot.com/lessons/Bitwise_Operators/Sum_using_only_bitwise_ops.jsp
     */
    public static int sumPositives(int num1, int num2) {

        if (num1 < 0)
            throw new NegativeNumberException("num1 is not a positive number");

        else if (num2 < 0)
            throw new NegativeNumberException("num2 is not a positive number");

        return sum(num1, num2);
    }
}
